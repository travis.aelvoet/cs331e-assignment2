# Travis Aelvoet
# tja875
import random
from flask import Flask, render_template, request, redirect, url_for, jsonify
mport json
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'},
         {'title': 'Algorithm Design', 'id': '2'},
         {'title': 'Python', 'id': '3'}]


def getNewID():
    for book in books:
        if book["title"] is None:
            return book['id']
    return len(books)+1


def insertBook(name):
    # Look for empty slot
    for book in books:
        if book["title"] is None:
            books[int(book['id'])-1] = {'title': name, 'id': book['id']}
            return
    # Append to end if no empty slots found
    books.append({'title': name, 'id': len(books)+1})


@app.route('/book/JSON')
def bookJSON():
    return jsonify(books)


@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)


@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        insertBook(request.form['name'])
        return redirect(url_for('showBook'))
    else:
        return render_template('newBook.html')


@app.route('/book/<int:book_id>/edit/', methods=['GET', 'POST'])
def editBook(book_id):
    if request.method == 'POST':
        books[book_id-1]["title"] = request.form['name']
        return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html', bookName=books[book_id-1]['title'], book_id=book_id)


@app.route('/book/<int:book_id>/delete/', methods=['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        books[book_id-1]['title'] = None
        return redirect(url_for('showBook'))
    else:
        return render_template('deleteBook.html', bookName=books[book_id-1]['title'], book_id=book_id)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
